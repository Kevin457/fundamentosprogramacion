declarar arreglos en Go
Para declarar arreglos en Go se necesita conocer la cantidad de miembros de la colecci�n y su tipo de dato. El formato de declaraci�n es el siguiente:

var nombre_de_variable [TAMA�O] tipo_de_dato

El nombre del arreglo puede ser cualquier identificador v�lido de Go, el tama�o cualquier n�mero entero mayor a 0, y el tipo de dato cualquiera v�lido de Go. Supongamos que deseamos almacenar los d�as de la semana en un arreglo de string llamado dias_semana:

var dias_semana [7] string

Inicializar un arreglo en Go
Los arreglos pueden ser inicializados elemento por elemento con sentencias de asignaci�n:

var dias_semana [0] = "domingo"
var dias_semana [1] = "lunes"
var dias_semana [2] = "martes"
var dias_semana [3] = "mi�rcoles"
var dias_semana [4] = "jueves"
var dias_semana [5] = "viernes"
var dias_semana [6] = "s�bado"
Que se note: En el ejemplo, las posiciones en el arreglo inician desde 0 y terminan hasta 6, es decir, son 7 elementos (como se especific� en la declaraci�n). A ese n�mero que se utiliza entre corchetes se le conoce como �ndice y es el que nos proporciona el acceso a las posiciones del arreglo con mucha facilidad. Los arreglos siempre inician a partir del �ndice 0, y por ende, el �ltimo �ndice es el tama�o del arreglo menos 1.

Tambi�n se pueden inicializar al momento de la declaraci�n:

var dias_semana = [] string{"domingo","lunes",
"martes", "mi�rcoles", "jueves", "viernes","s�bado"}