Una funci�n es un bloque de c�digo con un nombre asociado, que recibe cero o m�s argumentos como entrada, sigue una secuencia de sentencias, la cuales ejecuta una operaci�n deseada y devuelve un valor y/o realiza una tarea, este bloque puede ser llamados cuando se necesite.

El uso de funciones es un componente muy importante del paradigma de la programaci�n llamada estructurada, y tiene varias ventajas:

modularizaci�n: permite segmentar un programa complejo en una serie de partes o m�dulos m�s simples, facilitando as� la programaci�n y el depurado.
reutilizaci�n: permite reutilizar una misma funci�n en distintos programas.