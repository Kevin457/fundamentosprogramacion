nicializar un arreglo en la declaraci�n
Como ya se ha visto, C++ permite inicializar las variables en el momento de su declaraci�n. Lo mismo puede hacerse con un arreglo, especificando los valores iniciales coloc�ndolos entre llaves. Por ej:  int valores[5] = {100, 200, 300, 400, 500 };

Si no se especifica el tama�o de un arreglo que se inicializa en una declaraci�n, C++ asignar� la suficiente memoria para que contenga el n�mero de valores especificados, por ejemplo, la siguiente declaraci�n crea un arreglo capaz de guardar cuatro valores enteros:

int numeros[ ] = {1, 2, 3, 4 };

Las funciones trabajan con arreglos
Una funci�n puede inicializar al arreglo, operar con sus valores o mostrarlos por pantalla.

Para que la funci�n trabaje con el arreglo se debe especificar su tipo, no necesariamente su tama�o. Normalmente se pasar� un par�metro que especifique el n�mero de elementos que hay en el arreglo.

El programa 32ARR_FUN.CPP pasa los arreglos a la funci�n �mostrar_arreglo�, que presenta los valores por pantalla:

#include <iostream.h>
 
void mostrar_arreglo(int arreglo[], int nro_elementos)
 
{
 
int i;
 
for (i=0; i < nro_elementos; i++)
 
cout << arreglo[i] << ' ';
 
}
 
main()
 
{
 
int nros_chicos[5] = {1, 2, 3, 4, 5};
 
int nros_grandes[3] = {1000, 2000, 3000};
 
mostrar_arreglo(nros_chicos, 5);
 
mostrar_arreglo(nros_grandes, 3);
 
}

ARREGLOS MULTIDIMENSIONALES
 

Veamos el programa 34MULTIARR.CPP como un ejemplo del trabajo con arreglos multidimensionales (matrices):


#include <iostream.h>
 
#include <stdio.h>
 
&nbsp;
 
main()
 
{
 
int i,j;
 
int arr1[8][8];
 
int arr2[25][12];
 
&nbsp;
 
for (i = 0;i < 8;i++)
 
for (j = 0;j < 8;j++)
 
arr1[i][j] = i * j;       // Esto es una tabla de multiplicar
 
&nbsp;
 
for (i = 0;i < 25;i++)
 
for (j = 0;j < 12;j++)
 
arr2[i][j] = i + j;           // Esto es una tabla de suma
 
arr1[2][6] = arr2[24][10]*22;
 
arr1[2][2] = 5;
 
arr1[arr1[2][2]][arr1[2][2]] = 177;     // esto es arr1[5][5] = 177;
 
&nbsp;
 
for (i = 0;i < 8;i++) {
 
for (j = 0;j < 8;j++)
 
printf("%5d ",arr1[i][j]);
 
printf("\n");               // nueva l�nea para cada suma de i
 
}
 
}
La variable �arr1� es un arreglo de 8�8, que contiene 8 veces 8, o 64 elementos en total. El primer elemento es �arr1[0][0]�, y el �ltimo �arr1[7][7]�. Otro arreglo, �arr2� es tambi�n de este tipo, pero no es cuadrado, para que se vea que un arreglo multidimensionado no debe ser necesariamente cuadrado. Ambos arreglos est�n rellenos con datos, que representan una tabla de multiplicar, y, una tabla de sumar, el otro.

Para ilustrar que elementos individuales pueden ser modificados, a uno de los elementos de �arr1� se le asigna uno de los elementos de �arr2�, tras ser multiplicado por 22 (l�nea 18). En la siguiente l�nea, se le asigna a �arr1[2][2]� el valor arbitrario 5, y se lo usa para el sub�ndice del siguiente mandato de asignaci�n. El tercer mandato de asignaci�n es en realidad �arr1[5][5] = 177�, porque cada uno de los sub�ndices contiene el valor 5. Esto sirve a t�tulo de ilustraci�n de que cualquier expresi�n valida puede usarse como sub�ndice, s�lo debe cumplir 2 reglas, una es que debe ser un entero (aunque un �char� num�rico tambi�n valdr�a), y la otra es que debe estar en el l�mite del sub�ndice del arreglo, y no sobrepasarlo.

El contenido total de la matriz �arr1� se imprime por pantalla, en forma de cuadrado, con lo cual podemos comprobar por los valores si el programa hace lo que imagin�bamos.