Agregar un elemento al arreglo
Puedes agregar un elemento a un arreglo NumPy usando el m�todo append () del m�dulo NumPy.

La sintaxis para a�adir es la siguiente:

numpy.append(array, value, axis)

Los valores se agregar�n al final del arreglo y se devolver� un nuevo ndarray con los valores nuevos y antiguos como se muestra en la imagen anterior.

El eje es un entero opcional a lo largo del cual se define c�mo se mostrar� el arreglo. Si no se especifica el eje, la estructura del arreglo se aplanar� como se ver� m�s adelante.

Considera el siguiente ejemplo donde primero se declara un arreglo y luego usamos el m�todo para a�adir m�s valores al arreglo:

import numpy

a = numpy.array([1, 2, 3])

newArray = numpy.append (a, [10, 11, 12])

print(newArray)

import numpy
 
a = numpy.array([1, 2, 3])
 
newArray = numpy.append (a, [10, 11, 12])
 
print(newArray)