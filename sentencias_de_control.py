2.2.5.1. Bucle while
Este bucle, se encarga de ejecutar una misma acci�n "mientras que" una determinada condici�n se cumpla. Ejemplo: Mientras que a�o sea menor o igual a 2012, imprimir la frase "Informes del A�o a�o".

2.2.5.2. Bucle for
El bucle for, en Python, es aquel que nos permitir� iterar sobre una variable compleja, del tipo lista o tupla:

1) Por cada nombre en mi_lista, imprimir nombre

mi_lista = ['Juan', 'Antonio', 'Pedro', 'Herminio'] 
for nombre in mi_lista: 
    print nombre
2) Por cada color en mi_tupla, imprimir color:

mi_tupla = ('rosa', 'verde', 'celeste', 'amarillo') 
for color in mi_tupla: 
    print color

