Sentencia if
La instrucci�n if es, por excelencia, la m�s utilizada para construir estructuras de control de flujo.

Primera Forma

Ahora bi�n, la sintaxis utilizada en la programaci�n de C++ es la siguiente:

if (condicion)
   {
       Set de instrucciones
   }
siendo "condicion" el lugar donde se pondr� la condici�n que se tiene que cumplir para que sea verdadera la sentencia y as� proceder a realizar el "set de instrucciones" o c�digo contenido dentro de la sentencia.

Sentencia switch
switch es otra de las instrucciones que permiten la construcci�n de estructuras de control. A diferencia de if, para controlar el flujo por medio de una sentencia switch se debe de combinar con el uso de las sentencias case y break.

muy �til en los casos de presentaci�n de menus.

Sintaxis:

switch (condici�n)
{
    case primer_caso:
         bloque de instrucciones 1
    break;

    case segundo_caso:
         bloque de instrucciones 2
    break;

    case caso_n:
         bloque de instrucciones n
    break;

    default: bloque de instrucciones por defecto
}

Sentencias For
for(contador; final; incremento)
{
    Codigo a Repetir;
}
donde:

contador es una variable num�rica
final es la condici�n que se evalua para finalizar el ciclo (puede ser independiente del contador)
incremento es el valor que se suma o resta al contador
Hay que tener en cuenta que el "for" evalua la condici�n de finalizaci�n igual que el while, es decir, mientras esta se cumpla continuaran las repeticiones.

Sentencias For
for(contador; final; incremento)
{
    Codigo a Repetir;
}
donde:

contador es una variable num�rica
final es la condici�n que se evalua para finalizar el ciclo (puede ser independiente del contador)
incremento es el valor que se suma o resta al contador
Hay que tener en cuenta que el "for" evalua la condici�n de finalizaci�n igual que el while, es decir, mientras esta se cumpla continuaran las repeticiones.