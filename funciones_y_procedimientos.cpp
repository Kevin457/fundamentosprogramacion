Funciones:
Las funciones son un conjunto de procedimiento encapsulados en un bloque, usualmente reciben par�metros, cuyos valores utilizan para efectuar operaciones y adicionalmente retornan un valor. Esta definici�n proviene de la definici�n de funci�n matem�tica la cual posee un dominio y un rango, es decir un conjunto de valores que puede tomar y un conjunto de valores que puede retornar luego de cualquier operaci�n.

Procedimientos:
Los procedimientos son b�sicamente lo un conjunto de instrucciones que se ejecutan sin retornar ning�n valor, hay quienes dicen que un procedimiento no recibe valores o argumentos, sin embargo en la definici�n no hay nada que se lo impida. En el contexto de C++ un procedimiento es b�sicamente una funci�n void que no nos obliga a utilizar una sentencia return.

Durante este art�culo hablaremos sobre procedimientos y funciones, los m�todos son parte de un tema diferente.