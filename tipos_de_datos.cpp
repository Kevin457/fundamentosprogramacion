Tipo de Dato
Descripci�n
N�mero de bytes t�pico
Rango
short
Entero corto
2
-32768 a 32767
int
Entero
4
-2147483648 a +2147483647
long
Entero largo
4
-2147483648 a +2147483647
char
Car�cter
1
-128 a 127


Tipo de Dato
Descripci�n
N�mero de bytes t�pico
Rango
signed short
Entero corto
2
-32768 a 32767
unsigned short
Entero corto sin signo
2
0 a 65535
signed int
Entero
4
-2147483648 a +2147483647
unsigned int
Entero sin signo
4
0 a 4294967295
signed long
Entero largo
4
-2147483648 a +2147483647
unsigned long
Entero largo sin signo
4
0 a 4294967295
signed char
Car�cter
1
-128 a 127
unsigned char
Car�cter sin signo
1
0 a 255

Tipo de Dato
Descripci�n
N�mero de bytes t�pico
Rango
float
Real (N�mero en coma flotante)
4
Positivos: 3.4E-38 a 3.4E38
Negativos: -3.4E-38 a -3.4E38
double
Real doble(N�mero en coma flotante de doble precisi�n)
8
Positivos: 1.7E-308 a 1.7E308
Negativos: -1.7E-308 a -1.7E308
long double
Real doble largo
10
Positivos: 3.4E-4932 a 1.1E4932
Negativos: -3.4E-4932 a -1.1E4932


Tipo de Dato
Descripci�n
N�mero de bytes t�pico
Rango
bool
Dato de tipo l�gico
1
0, 1


Tipo de Dato
Descripci�n
N�mero de bytes t�pico
Rango
wchar_t
Car�cter Unicode
2
0 a 65535