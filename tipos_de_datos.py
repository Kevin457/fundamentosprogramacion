Tipos de Enteros
Numeros:
Enteros
Reales
Complejos
Enteros:
Son los n�meros que no tienen decimales y pueden ser positivos y negativos (el 0 es un entero tambi�n). Estos n�meros son los conocidos int (entero) o long (entero largo para m�s precisi�n).

X=-4
Reales:
Son los n�meros que tienen decimales y son del tipo float.

X= 3.5502
Complejos:
Son los n�meros que tienen una parte real y una imaginaria. Estos n�meros se denominan complex y si no los conoces es probable que no lo necesites, aunque si te puedo de decir que tienen algunas aplicaciones muy interesantes. Ejemplo de n�mero completo:

X= 2,1 + 6j
 

Tipo de Cadenas
Las cadenas son texto cerrado entre comillas (simples o dobles). Las cadenas admiten operadores como la suma o la multiplicaci�n. Ejemplo de cadenas:

cadena1 = ('comillas simples')
print (cadena1)
cadena2 = ("comillas dobles")
print (cadena2)
n = "Aprender"
a = "Python"
n_a = n + " " + a
print (n_a)
 

Tipos de booleanos
Este es el tipo de variable que solo puede tener Verdadero o Falso. Son valores muy usados en condiciones y bucles. Ejemplo de variable booleana:

aT = True
print ("El valor es Verdadero:", aT, ", el cual es de tipo", type(aT)), "\n"
aF = False
print ("El valor es Falso:", aF, ", el cual es de tipo", type(aF))
 

Tipos de conjuntos
Son una colecci�n de datos sin elementos que se repiten

pla = 'pastelito', 'jamon', 'papa', 'empanadilla', 'mango', 'quesito'
print (pla)
 

Tipos de listas
Son listas que almacenan vectores (arrays). Estas listas pueden tener diferentes tipos de datos. Ejemplo de listas en Python:

b = ['2.36', 'elefante', 1010, 'rojo']
print (b)
l4 = b[0:3:2]
print(l4)
 

Tipos de tuplas:
Es una lista que no se puede modificar despu�s de su creaci�n, es inmodificable:

# Ejemplo simple
tupla = 19645, 59621, 'hola python!'
# Ejemplo tuplas anidadas
otra = tupla, (1, 5, 3, 6, 5)
# operaci�n asignaci�n de valores de una tupla en variables
x, y, z = tupla
print(tupla)
print(otra)
#ejemplo de tupla para una conexi�n con una base detos
print ("\nConectar a la base de datos MySql")
print ("==============================\n")
conexion_bd = "1546.540.07.18","accesoroot","1gh6","users",
print ("Conexion tipica:", conexion_bd)
print (conexion_bd)
conexion_c = conexion_bd, "3457","19",
print ("\nConexion con estos parametros:", conexion_c)
print (conexion_c)
print ("\n")
print ("Acceder a la IP de la base de datos:", conexion_c[0][0])
print ("Acceder al usuario de la base de datos:", conexion_c[0][1])
print ("Acceder a la clave de la base de datos:", conexion_c[0][2])
print ("Acceder al nombre de la base de datos:", conexion_c[0][3])
print ("Acceder al puerto :", conexion_c[1])
print ("Acceder al tiempo de espera de conexion:", conexion_c[2])
 

Tipos de diccionarios:
Define los datos uno a uno entre un campo y un valor, ejemplo:

datos_basicos = {
"nombres":"Fran",
"apellidos":"Pardo Garcia",
"numero":"145548",
"fecha_nacimiento":"03111980",
"lugar_nacimiento":"Madrid, Espa�a",
"nacionalidad":"Portuguesa",
"estado_civil":"Casado"}
print ("\nDetalle del diccionario")
print ("=======================\n")
print ("\nClaves del diccionario:", datos_basicos.keys())
print ("\nValores del diccionario:", datos_basicos.values())
print ("\nElementos del diccionario:", datos_basicos.items())
# Ejemplo practico de los diccionarios
print ("\nInscripcion de Curso")
print ("====================")
print ("\nDatos de participante")
print ("---------------------")
print ("Cedula de identidad:", datos_basicos['numero'])
print ("Nombre completo: " + datos_basicos['nombres'] + " " + datos_basicos['apellidos'])